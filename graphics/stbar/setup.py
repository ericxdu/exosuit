import os

from PIL import Image

def convert_pixels(file):
    image = file.convert("RGBA")
    width, height = image.size[0], image.size[1]
    for x in range(0, width):
        for y in range(0, height):
            pixel = image.getpixel((x, y))
            if pixel[3] == 255:
                image.putpixel((x, y), (255, 255, 255, 255))
    return image

image_st = convert_pixels(Image.open("stfst000.png"))
if not os.path.isdir("graphics"):
    os.mkdir("graphics")
for x in range(0, 5):
    for y in range(0, 3):
        image_st.save("graphics/stfst" + str(x) + str(y) + ".png", "PNG")

image_kill = convert_pixels(Image.open("stfkill0.png"))
for x in range(0, 5):
    image_kill.save("graphics/stfkill" + str(x) + ".png", "PNG")

image_evil = convert_pixels(Image.open("stfevl00.png"))
for x in range(0, 5):
    image_evil.save("graphics/stfevl" + str(x) + ".png", "PNG")

image_tl = convert_pixels(Image.open("stftl000.png"))
for x in range(0, 5):
    for y in range(0, 3):
        image_tl.save("graphics/stftl" + str(x) + str(y) + ".png", "PNG")

image_tr = convert_pixels(Image.open("stftr000.png"))
for x in range(0, 5):
    for y in range(0, 3):
        image_tr.save("graphics/stftr" + str(x) + str(y) + ".png", "PNG")
