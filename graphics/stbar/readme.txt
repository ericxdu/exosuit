An attempt to document the specific behaviors of StatusBar/HUD
graphics, and automatically generate panels, faces, and numerals from
source files. This will be moved to a separate branch.


The prefix ST refers to graphics related to the statusbar/HUD. STBAR is 
the background picture upon which other pictures are drawn. Normally it 
is drawn at coordinates 0, 168 and has dimensions 320, 32.

STARMS is drawn at coordinates 104, 168 and has dimensions 40, 32.

STF refers to the status bar "face" picture, including the colored 
background squares for multiplayer games. Subsets of the face picture 
include STFST, STFTL, STFTR, STFKILL, STFOUCH, STFGOD, and STFDEAD. 
These pictures are drawn at coordinates 144, 168.
