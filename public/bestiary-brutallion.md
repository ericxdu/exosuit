Brutallion Military Confederacy
===============================

Warrior-Officers
----------------

- Paladin (BOS2 Knight)
  + 100% muscle, these guys take at least three rocket blasts to take
  down, and while you’re trying they’ll shower you with energy projectiles.
- Warlord (BOSS Bruiser)
  + If the Paladin wasn’t tough enough, this one will take five rocket
  blasts.


Cybernetic War Constructs
-------------------------

- Chimera (FATT Fatso)
  + These genetically-engineered composites have been fitted with long
  distance flame throwers, practically making them into living,
  slithering tanks.
- Manticore (BSPI Baby)
  + These creatures have been equipped with polaric energy cannons,
  making them a deadly challenge.
- Dominion (SPID Spider)
  + This tank on legs is equipped with a rapid-fire minigun and will
  take a lot of effort to bring down.
- Throne (CYBR Cyborg)
  + The ultimate blend of military technology and genetic engineering,
  these three-legged creatures are fast-moving, heavily armored and
  equipped with a missile launcher that you’ll want to avoid.
