Datacenter Epsilon: Bestiary
============================


Biomechanical Constructs
------------------------

- Assault Tripod (CYBR Cyborg)
  + Cyborg missle platform. Created to serve as artillery support.
  Many have been abandoned and gone rogue. Anti-explosive armor makes
  it vulnerable only to direct damage.
- Battle Brain (BBRN BossBrain)
  + Bio-engineered super-intelligent war coordinator. It is designed
  to help command large forces on complicated battlegrounds.
  Destroying it can be key to victory.
- Flamethrower (FATT Fatso)
  + Large war construct equipped with long-range fireball generators.
  Rogue flamethrowers attack on sight with a volley of attacks.
  Approach with caution.
- Keen Esperpod (KEEN Keen)
  + Extra-sensory organisms kept in suspended animation to form a
  security network. Disabling all of them in an area can be key to
  unlocking the way forward.
- Manticore (BSPI Baby)
  + Smaller version of the larger walker. Equipped with an ion
  cannon and often deployed in greater numbers. This model lacks
  the armor of its larger counterpart.
- Spiderwalker (SPID Spider)
  + Large six-legged war construct equipped with a vulcan cannon.
  Simple programming causes it to attack anything identified as a
  threat. Advanced armor makes it immune to any indirect explosive
  damage.


Brutallion Warriors
-------------------

Incredibly resilient warriors of the bru'al species, the painbringer is 
biotechnically enhanced with armor, weapons, and dense musculature. They 
often fight in pairs, and can even work together in large groups or 
under the command of the stronger pain lords. They can fight in close 
quarters using a scratch attack, but they favor their long-range comet 
launchers. Painbringers and pain lords will not knowingly attack one 
another, and they are immune to each other's projectiles.

- Pain Lord (BOSS Bruiser)
  + Military commanders in the Brutallion Confederacy. Physically
  strong and hardy, bolstered by cybernetic and chemical
  enhancements. Reptoid grafted to arm spits balls of acid.
- Painbringer (BOS2 Knight)
  + Brutallion generals. Have the same armaments as pain lords, but
  are less physically imposing.


Imperial Revenants
------------------

Former members and servants of the now-defunct Thanoperium empire, who
seeded the galaxy with their minions centuries ago.

- Dark Ranger (SKEL Undead)
  + Not much is known about this magician, who often waits in ambush
  either alone or in groups. They roam the galaxy after their own
  enigmatic purposes, and are capable combatants. They can fire guided
  and unguided lava rockets, but will often switch tactics and close to
  melee range striking with their fists, only to counterattack with a
  devastating close-range rocket.
- Deadflare (SKUL Skull)
- Flamebringer (VILE Vile)
- Necromancer (VILE Vile)
  + Descended from the ancient Thanoperium, the necromancer is feared
  by all species on the battlefield. Even war constructs will not
  knowingly harm these creatures. A necromancer will often fortify a
  strategic location and raise an army, supporting their gathered
  warriors by reviving them if they are felled. However, this proud
  magician will not hesitate to destroy any creature that attacks it by
  casting a devastating fire bomb.
- Summoner (PAIN Pain)
  + The summoner manufactures Deadflare swarms. It deploys a single
  deadflare at a time, intending to overwhelming its enemies over time.
  Destroying it will release any deadflare inside that were being
  manufactured, but it will not be making any more after that. A large
  swarm of deadflare near a source of thanatos may be able to
  create a new summoner, thereby ensuring their continued existence.


Xenobiological Fauna
--------------------

- Cloak Worm (SARG Shadows)
- Hatchling (SKUL Skull)
- Matribite (PAIN Pain)
- Serpentipede (TROO Troop)
- Trilobite (HEAD Head)
- Vore Worm (SARG Sergeant)


Spacer Mercenaries
------------------

Hired by warlords or working for themselves to plunder items of value, 
these are soldiers for hire divided into strategic ranks of Grunt, 
Commando, Gunner, and Sentry. Being on no particular side, they'll
often oppose each other on the battlefield and mistrust each other as
often as they work together. Spacers have a physiology that evolved to
protect vital organs from vacuum and other harsh environments.

- Grunt (POSS Possessed)
  + Low-ranking mercenary soldiers. Often working for competing
  interests. As likely to find them opposing each other on the
  battlefield as working together.
- Commando (SPOS Shotguy)
  + Mercenary special forces. Usually equipped with a tactical
  shotgun for close range strikes. Engage with caution from a
  distance.
- Gunner (CPOS Chainguy)
  + Most formidable of the battlefield mercenaries. Equipped with a
  minigun. Extremely dangerous.
- Sentry (SSWV WolfSS)
  + Rarely seen on the battlefield, this rank of mercenary is deployed
  in a defensive role inside secret compounds.


External Links
--------------

- [Datacenter on OpenGameArt](https://opengameart.org/node/127293/)
